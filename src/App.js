import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Header } from './components/layout/header/Header'
import { Footer } from './components/layout/footer/Footer'
import { 
    UserListPage, UserFormPage, UserFormEditPage,
    ProductListPage, ProductFormPage, ProductFormEditPage,
    BrandListPage, BrandFormPage, BrandFormEditPage,
    CompanyListPage, CompanyFormPage, CompanyFormEditPage,
    LoginFormPage
} from './components/pages'

function App() {
  return (
    <>
    <Router>
        <Header />
            <Switch>
                <Route path='/user-list'><UserListPage /></Route>
                <Route path='/user-form'><UserFormPage /></Route>
                <Route path='/user-form-edit/:id'><UserFormEditPage /></Route>

                <Route path='/product-list'><ProductListPage /></Route>
                <Route path='/product-form'><ProductFormPage /></Route>
                <Route path='/product-form-edit/:id'><ProductFormEditPage /></Route>

                <Route path='/brand-list'><BrandListPage /></Route>
                <Route path='/brand-form'><BrandFormPage /></Route>
                <Route path='/brand-form-edit/:id'><BrandFormEditPage /></Route>

                <Route path='/company-list'><CompanyListPage /></Route>
                <Route path='/company-form'><CompanyFormPage /></Route>
                <Route path='/company-form-edit/:id'><CompanyFormEditPage /></Route>

                <Route path='/login'><LoginFormPage /></Route>
            </Switch>
        <Footer />  
    </Router>
    </>
  );
}

export default App;