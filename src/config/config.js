import axios from 'axios'
import { getToken } from './auth'

const http = axios.create({
    baseURL: 'https://action-figuresapi.herokuapp.com'
})

if (getToken) {
    http.defaults.headers['x-auth-token'] = getToken()
}

export { http }