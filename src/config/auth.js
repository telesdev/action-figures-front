const tokenKey = 'x-auth-token'

const getToken = () => {
    localStorage.getItem(tokenKey)
}

const saveToken = (tokenValue) => {
    localStorage.setItem(tokenKey, tokenValue)
}

const deleteToken = () => {
    localStorage.removeItem(tokenKey)
}

export { getToken, saveToken, deleteToken }