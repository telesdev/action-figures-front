import { http } from '../config/config'

const authUser = (data) => http.post('/auth', data)

export { authUser }