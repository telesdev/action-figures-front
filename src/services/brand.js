import { http } from '../config/config'

const fetchBrands = () => http.get('/brand')

const detailBrand = (id) => http.get(`/brand/${id}`)

const createBrand = (data) => http.post('/brand', data)

const updateBrand = (data) => http.patch(`/brand/${data._id}`, data)

const deleteBrand = (id) => http.delete(`/brand/${id}`)

export { fetchBrands, createBrand, updateBrand, deleteBrand, detailBrand }
