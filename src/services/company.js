import { http } from '../config/config'

const fetchCompanies = () => http.get('/company')

const detailCompany = (id) => http.get(`/company/${id}`)

const createCompany = (data) => http.post('/company', data)

const updateCompany = (data) => http.patch(`/company/${data._id}`, data)

const deleteCompany = (id) => http.delete(`/company/${id}`)

export { fetchCompanies, detailCompany, createCompany, updateCompany, deleteCompany }
