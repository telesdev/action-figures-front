import { http } from '../config/config'

const fetchUsers = () => http.get('/user')

const detailUser = (id) => http.get(`/user/${id}`)

const createUser = (data) => http.post('/user', data)

const updateUser = (data) => http.patch(`/user/${data._id}`, data)

const deleteUser = (id) => http.delete(`/user/${id}`)

export { fetchUsers, createUser, updateUser, deleteUser, detailUser }