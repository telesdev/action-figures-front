import { http } from '../config/config'

const fetchProducts = () => http.get('/product')

const detailProduct = (id) => http.get(`/product/${id}`)

const createProduct = (data) => http.post('/product', data)

const updateProduct = (data) => http.patch(`/product/${data._id}`, data)

const deleteProduct = (id) => http.delete(`/product/${id}`)

export { fetchProducts, detailProduct, createProduct, updateProduct, deleteProduct }