import React, { useState } from 'react'
import { Button, Form, Container } from 'react-bootstrap';
import { authUser } from '../../services/auth'
import { saveToken } from '../../config/auth'
import { http } from '../../config/config'
import { useHistory } from 'react-router-dom';
import './login.css'


export const LoginForm = () => {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const dados = { email, password }

    const history = useHistory()

    const login = async () => {
        try {
            const data = await authUser(dados)
            http.defaults.headers['x-auth-token'] = data.data.token
            saveToken(data.data.token)
            history.push('/')
        } catch (err){
            console.log(err.response.data)
            throw err
        }
    }

    return (
        <>
        <Container id='loginFormContainer'>
            <Form>
                <Form.Group>
                    <Form.Label>Email:</Form.Label>
                    <Form.Control type='email' placeholder='Insira seu email' value={email || ''} onChange={e => setEmail(e.target.value)} name='email'/>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Senha:</Form.Label>
                    <Form.Control type='password' placeholder='Insira sua senha' value={password || ''} onChange={e => setPassword(e.target.value)} name='password'/>
                </Form.Group>
                <Button className='btnLogin' onClick={login}>Entrar</Button>
                <Button className='btnLogin' onClick={e => alert(`Você digitou Email: '${email}' e Senha: '${password}'`)}>Esqueci minha senha</Button>
            </Form>
        </Container>
        </>
    )
}