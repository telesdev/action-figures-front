import React from 'react'
import { UserFormEdit } from '../../user/UserFormEdit'
import '../../user/user.css'

export const UserFormEditPage = () => {
    return(
        <>
        <h2 id='userFormTitle'>Formulário de Usuários - Editar</h2>
        <UserFormEdit />
        </>
    )
}