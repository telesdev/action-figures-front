import React from 'react'
import { CompanyFormEdit } from '../../company/CompanyFormEdit'
import '../../company/company.css'

export const CompanyFormEditPage = () => {
    return(
        <>
        <h2 id='companyFormTitle'>Formulário de Fabricantes - Editar</h2>
        <CompanyFormEdit />
        </>
    )
}