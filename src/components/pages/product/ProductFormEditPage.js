import React from 'react'
import { ProductFormEdit } from '../../product/ProductFormEdit'
import '../../product/product.css'

export const ProductFormEditPage = () => {
    return(
        <>
        <h2 id='productFormTitle'>Formulário de Produtos - Editar</h2>
        <ProductFormEdit />
        </>
    )
}