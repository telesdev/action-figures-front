import React from 'react'
import { BrandFormEdit } from '../../brand/BrandFormEdit'
import '../../brand/brand.css'

export const BrandFormEditPage = () => {
    return(
        <>
        <h2 id='brandFormTitle'>Formulário de Marcas - Editar</h2>
        <BrandFormEdit />
        </>
    )
}