import React from 'react'
import { LoginForm } from '../../login/LoginForm'
import '../../login/login.css'

export const LoginFormPage = () => {
    return(
        <>
        <h2 id='loginFormTitle'>Formulário de Login</h2>
        <LoginForm />
        </>
    )
}

