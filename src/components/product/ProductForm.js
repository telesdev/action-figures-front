import React, { useEffect, useState } from 'react'
import { createProduct } from '../../services/product'
import { fetchBrands } from '../../services/brand'
import { fetchCompanies } from '../../services/company'
import { Button, Form, Container } from 'react-bootstrap';
import './product.css'
import { useHistory } from 'react-router-dom';

export const ProductForm = () => {
    
    const [name, setName] = useState('')
    const [brands, setBrands] = useState([])
    const [companies, setCompanies] = useState([])
    const [description, setDescription] = useState('')
    const [type, setType] = useState('')
    const [price, setPrice] = useState('')
    const [features, setFeatures] = useState('')
    const [content, setContent] = useState('')
    
    const form = { name, brands, companies, description, type, price, features, content }

    const history = useHistory()

    useEffect(() => {
        const getItem = async () => {
            const { data }  = await fetchBrands()
            setBrands(data)
        }
        getItem()
    }, [])

    useEffect(() => {
        const getItem = async () => {
            const { data }  = await fetchCompanies()
            setCompanies(data)
        }
        getItem()
    }, [])

    const post = async () => {
        try {
            await createProduct(form)
            alert('Produto adicionado com sucesso')
            history.push('/product-list')
        } catch (err){
            console.log(err.response.data)
            throw err
        }
    }

    return (
        <>
        <Container id='productFormContainer'>
            <Form>
                <Form.Group>
                    <Form.Label>Nome:</Form.Label>
                    <Form.Control type='text' name='name' placeholder='Entre com o seu nome' value={name} onChange={e => setName(e.target.value)}/>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Marca:</Form.Label>
                    <Form.Control as='select' name='brand' placeholder='Escolha a marca do produto' onChange={e => setBrands(e.target.value)}>
                        {brands.map(brand => <option key={brand._id} value={brand._id}>{brand.name}</option>)}
                    </Form.Control>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Fabricante:</Form.Label>
                    <Form.Control as='select' name='company' placeholder='Escolha o fabricante do produto' onChange={e => setCompanies(e.target.value)}>
                        {companies.map(company => <option key={company._id} value={company._id}>{company.name}</option>)}
                    </Form.Control>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Descrição:</Form.Label>
                    <Form.Control type='text' name='description' placeholder='Entre com a descrição' value={description} onChange={e => setDescription(e.target.value)} />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Versão:</Form.Label>
                    <Form.Check name='type' type='radio' label='Standard' value={type} onChange={e => setType(e.target.label)}/>
                    <Form.Check name='type' type='radio' label='Collector' value={type} onChange={e => setType(e.target.label)}/>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Valor:</Form.Label>
                    <Form.Control type='number' name='price' placeholder='Entre com o valor' value={price} onChange={e => setPrice(e.target.value)} />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Tamanho:</Form.Label>
                    <Form.Control type='text' name='features' placeholder='' value={features.size} onChange={e => setFeatures(e.target.value)} />
                    <Form.Label>Material:</Form.Label>
                    <Form.Control type='text' name='features' placeholder='' value={features.material} onChange={e => setFeatures(e.target.value)} />
                    <Form.Label>Quantidade:</Form.Label>
                    <Form.Control type='text' name='features' placeholder='' value={features.quantity} onChange={e => setFeatures(e.target.value)} />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Conteúdo:</Form.Label>
                    <Form.Control type='text' name='content' placeholder='Entre com o conteúdo' value={content} onChange={e => setContent(e.target.value)} />
                </Form.Group>
                <Button onClick={post}>Cadastrar</Button> 
            </Form>
        </Container>
        </>
    )
}