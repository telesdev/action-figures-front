import React, { useState } from 'react'
import { deleteProduct } from '../../services/product'
import { Modal, Button } from 'react-bootstrap'
import { useHistory } from 'react-router-dom'
import './product.css'

export const ProductListItem = ({ product }) => {

    const [show, setShow] = useState(false)

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const history = useHistory()

    const update = async () => history.push(`/product-form-edit/${product._id}`)

    const deletar = async () => {
        try {
            await deleteProduct(product._id)
            handleClose()
        }
        catch (err) {
            console.log(err.response.data)
            throw err
        }
    }

    return (
        <>
        <tr>
            <td>{product.name}</td>
            <td>{product.brand.name}</td>
            <td>{product.company.name}</td>
            <td>{product.description}</td>
            <td>{product.type}</td>
            <td>{product.price}</td>
            <td>{product.features.size}</td>
            <td>{product.features.material}</td>
            <td>{product.features.quantity}</td>
            <td>{product.content}</td>
            <td><Button onClick={update} variant='primary'>Editar</Button> <Button onClick={handleShow} variant='danger'>Deletar</Button></td>
        </tr>
        <Modal show={show} onHide={handleClose} centered backdrop='static' keyboard={false}>
            <Modal.Body>
                <p>Deseja excluir o produto {product.name}?</p>
            </Modal.Body>

            <Modal.Footer>
                <Button variant="secondary" onClick={() => deletar({ product : product })}>Sim</Button>
                <Button variant="primary" onClick={handleClose}>Não</Button>
            </Modal.Footer>
        </Modal>
        </>
    )
}