import React, { useState } from 'react'
import { createBrand } from '../../services/brand'
import { useHistory } from 'react-router-dom';
import { Button, Form, Container } from 'react-bootstrap';
import './brand.css'

export const BrandForm = () => {
    
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')

    const form = { name, description }

    const history = useHistory()

    const post = async () => {
        try {
            await createBrand(form)
            alert('Fabricante adicionado com sucesso')
            history.push('/brand-list')
        } catch (err){
            console.log(err.response.data)
            throw err
        }
    }

    return (
        <>
        <Container id='brandFormContainer'>
            <Form>
                <Form.Group>
                    <Form.Label>Nome:</Form.Label>
                    <Form.Control type='text' name='name' placeholder='Entre com o nome da marca' value={name} onChange={e => setName(e.target.value)} />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Descrição:</Form.Label>
                    <Form.Control type='text' name='description' placeholder='Entre com a descrição da marca' value={description} onChange={e => setDescription(e.target.value)} />
                </Form.Group>
                <Button onClick={post}>Cadastrar</Button> 
            </Form>
        </Container>
        </>
    )
}