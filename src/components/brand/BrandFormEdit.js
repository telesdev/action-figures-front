import React, { useEffect, useState } from 'react'
import { detailBrand, updateBrand } from '../../services/brand'
import { useHistory, useParams } from 'react-router-dom';
import { Button, Form, Container } from 'react-bootstrap';
import './brand.css'

export const BrandFormEdit = () => {
    
    const [brand, setBrand] = useState({})
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')

    const form = { name, description }

    const history = useHistory()

    const query = useParams()

    useEffect(() => {
        const getItem = async () => {
            const { data } = await detailBrand(query.id)
            setBrand(data)
        }
        getItem()
    }, [query])

    const update = async () => {
        try {
            for (const [key, value] of Object.entries(form)) {
                if (!value) {
                    form[key] = brand[key]
                }
            }
            await updateBrand(form)
            alert('Fabricante editado com sucesso')
            history.push('/brand-list')
        } catch (err){
            console.log(err.response.data)
            throw err
        }
    }

    return (
        <>
        <Container id='brandFormContainer'>
            <Form>
                <Form.Group>
                    <Form.Label>Nome:</Form.Label>
                    <Form.Control type='text' name='name' placeholder={brand.name} value={name} onChange={e => setName(e.target.value)} />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Descrição:</Form.Label>
                    <Form.Control type='text' name='description' placeholder={brand.description} value={description} onChange={e => setDescription(e.target.value)} />
                </Form.Group>
                <Button onClick={update}>Cadastrar</Button> 
            </Form>
        </Container>
        </>
    )
}