import React, { useState } from 'react'
import { Button, Modal } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { deleteBrand } from '../../services/brand'
import './brand.css'

export const BrandListItem = ({ brand }) => {
      
    const [show, setShow] = useState(false)

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const history = useHistory()

    const update = async () => history.push(`/brand-form-edit/${brand._id}`)

    const deletar = async () => {
        try {
            await deleteBrand(brand._id)
            handleClose()
        }
        catch (err) {
            console.log(err.response.data)
            throw err
        }
    }

    return (
        <>
        <tr>
            <td>{brand.name}</td>
            <td>{brand.description}</td>
            <td><Button onClick={update} variant='primary'>Editar</Button> <Button onClick={handleShow} variant='danger'>Deletar</Button></td>
        </tr>
        <Modal show={show} onHide={handleClose} centered backdrop='static' keyboard brand={brand}>
            <Modal.Body>
                <p>Deseja excluir a marca {brand.name}?</p>
            </Modal.Body>

            <Modal.Footer>
                <Button variant="secondary" onClick={() => deletar({ brand : brand })}>Sim</Button>
                <Button variant="primary" onClick={handleClose}>Não</Button>
            </Modal.Footer>
        </Modal>        
        </>
    )
}