import React, { useEffect, useState } from 'react'
import { detailUser, updateUser } from '../../services/user'
import { Button, Form, Container } from 'react-bootstrap';
import './user.css'
import { useHistory, useParams } from 'react-router-dom';

export const UserFormEdit = () => {
    
    const [user, setUser] = useState({})
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const form = { name, email, password }

    const history = useHistory()

    const query = useParams()

    useEffect(() => {
        const getItem = async () => {
            const { data }  = await detailUser(query.id)
            setUser(data)
        }
        getItem()
    }, [query])
    
    const update = async () => {
        try {
            for (const [key, value] of Object.entries(form)) {
                if (!value) {
                    form[key] = user[key]
                }
            }
            await updateUser(form)
            alert('Usuário editado com sucesso')
            history.push('/user-list')
        } catch (err){
            console.log(form)
            console.log(err.response.data)
            throw err
        }
    }

    return (
        <>
        <Container id='userFormContainer'>
            <Form>
                <Form.Group>
                    <Form.Label>Nome:</Form.Label>
                    <Form.Control type='text' name='name' placeholder={user.name} value={name} onChange={e => setName(e.target.value)} />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Email:</Form.Label>
                    <Form.Control type='email' name='email' placeholder={user.email} value={email} onChange={e => setEmail(e.target.value)} />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Senha:</Form.Label>
                    <Form.Control type='password' name='password' placeholder={user.password} value={password} onChange={e => setPassword(e.target.value)} />
                </Form.Group>
                <Button onClick={update}>Atualizar</Button> 
            </Form>
        </Container>
        </>
    )
}