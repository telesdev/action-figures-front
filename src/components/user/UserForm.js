import React, { useState } from 'react'
import { createUser } from '../../services/user'
import { Button, Form, Container } from 'react-bootstrap';
import './user.css'
import { useHistory } from 'react-router-dom';

export const UserForm = () => {
    
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    
    const form = { name, email, password }
    
    const history = useHistory()

    const post = async () => {
        try {
            await createUser(form)
            alert('Usuário adicionado com sucesso')
            history.push('/user-list')
        } catch (err){
            console.log(err.response.data)
            throw err
        }
    }

    return (
        <>
        <Container id='userFormContainer'>
            <Form>
                <Form.Group>
                    <Form.Label>Nome:</Form.Label>
                    <Form.Control type='text' name='name' placeholder='Entre com o seu nome' value={name} onChange={e => setName(e.target.value)} />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Email:</Form.Label>
                    <Form.Control type='email' name='email' placeholder='Entre com o seu email' value={email} onChange={e => setEmail(e.target.value)} />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Senha:</Form.Label>
                    <Form.Control type='password' name='password' placeholder='Entre com a sua senha' value={password} onChange={e => setPassword(e.target.value)} />
                </Form.Group>
                <Button onClick={post}>Cadastrar</Button> 
            </Form>
        </Container>
        </>
    )
}