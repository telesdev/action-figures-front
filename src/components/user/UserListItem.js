import React, { useState } from 'react'
import { deleteUser } from '../../services/user'
import { Modal, Button } from 'react-bootstrap'
import './user.css'
import { useHistory } from 'react-router-dom'

export const UserListItem = ({ user }) => {
      
    const [show, setShow] = useState(false)

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const history = useHistory()

    const update = async () => history.push(`/user-form-edit/${user._id}`)

    const deletar = async () => {
        try {
            await deleteUser(user._id)
            handleClose()
        }
        catch (err) {
            console.log(err.response.data)
            throw err
        }
    }

    return (
        <>
        <tr>
            <td>{user.name}</td>
            <td>{user.email}</td>
            <td><Button onClick={update} variant='primary'>Editar</Button> <Button onClick={handleShow} variant='danger'>Deletar</Button></td>
        </tr>
        <Modal show={show} onHide={handleClose} centered backdrop='static' keyboard={false}>
            <Modal.Body>
                <p>Deseja excluir o usuário {user.name}?</p>
            </Modal.Body>

            <Modal.Footer>
                <Button variant="secondary" onClick={() => deletar({ user : user })}>Sim</Button>
                <Button variant="primary" onClick={handleClose}>Não</Button>
            </Modal.Footer>
        </Modal>
        </>
    )
}