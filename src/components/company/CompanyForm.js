import React, { useState } from 'react'
import { createCompany } from '../../services/company'
import { Button, Form, Container } from 'react-bootstrap';
import './company.css'
import { useHistory } from 'react-router-dom';

export const CompanyForm = () => {
    
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [address, setAddress] = useState('')

    const form = { name, description, address }

    const history = useHistory()

    const post = async () => {
        try {
            await createCompany(form)
            alert('Fabricante adicionado com sucesso')
            history.push('/company-list')
        } catch (err){
            console.log(err.response.data)
            throw err
        }
    }

    return (
        <>
        <Container id='companyFormContainer'>
        <Form>
                <Form.Group>
                    <Form.Label>Nome:</Form.Label>
                    <Form.Control type='text' name='name' placeholder='Entre com o nome do fabricante' value={name} onChange={e => setName(e.target.value)} />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Descrição:</Form.Label>
                    <Form.Control type='text' name='description' placeholder='Entre com a descrição do fabricante' value={description} onChange={e => setDescription(e.target.value)} />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Endereço:</Form.Label>
                    <Form.Control type='text' name='address' placeholder='Entre com o endereço do fabricante' value={address} onChange={e => setAddress(e.target.value)} />
                </Form.Group>
                <Button onClick={post}>Cadastrar</Button> 
            </Form>
        </Container>
        </>
    )
}