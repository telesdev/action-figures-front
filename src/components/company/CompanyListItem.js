import React, { useState } from 'react'
import { Button, Modal } from 'react-bootstrap'
import { useHistory } from 'react-router-dom'
import { deleteCompany } from '../../services/company'
import './company.css'

export const CompanyListItem = ({ company }) => {

    const [show, setShow] = useState(false)

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const history = useHistory()

    const update = async () => history.push(`/company-form-edit/${company._id}`)

    const deletar = async () => {
        try {
            await deleteCompany(company._id)
            handleClose()
        }
        catch (err) {
            console.log(err.response.data)
            throw err
        }
    }    

    return (
        <>
        <tr>
            <td>{company.name}</td>
            <td>{company.description}</td>
            <td>{company.address}</td>
            <td><Button onClick={update} variant='primary'>Editar</Button> <Button onClick={handleShow} variant='danger'>Deletar</Button></td>
        </tr>
        <Modal show={show} onHide={handleClose} centered backdrop='static' keyboard={false}>
            <Modal.Body>
                <p>Deseja excluir o fabricante {company.name}?</p>
            </Modal.Body>

            <Modal.Footer>
                <Button variant="secondary" onClick={() => deletar({ company : company })}>Sim</Button>
                <Button variant="primary" onClick={handleClose}>Não</Button>
            </Modal.Footer>
        </Modal>
        </>
    )
}