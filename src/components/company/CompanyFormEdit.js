import React, { useEffect, useState } from 'react'
import { detailCompany, updateCompany } from '../../services/company'
import { Button, Form, Container } from 'react-bootstrap';
import './company.css'
import { useHistory, useParams } from 'react-router-dom';

export const CompanyFormEdit = () => {
    
    const [company, setCompany] = useState({})
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [address, setAddress] = useState('')

    const form = { name, description, address }

    const history = useHistory()

    const query = useParams()

    useEffect(() => {
        const getItem = async () => {
            const { data }  = await detailCompany(query.id)
            setCompany(data)
        }
        getItem()
    }, [query])
    
    const update = async () => {
        try {
            for (const [key, value] of Object.entries(form)) {
                if (!value) {
                    form[key] = company[key]
                }
            }
            await updateCompany(form)
            alert('Fabricante editado com sucesso')
            history.push('/company-list')
        } catch (err){
            console.log(err.response.data)
            throw err
        }
    }

    return (
        <>
        <Container id='companyFormContainer'>
        <Form>
                <Form.Group>
                    <Form.Label>Nome:</Form.Label>
                    <Form.Control type='text' name='name' placeholder={company.name} value={name} onChange={e => setName(e.target.value)} />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Descrição:</Form.Label>
                    <Form.Control type='text' name='description' placeholder={company.description} value={description} onChange={e => setDescription(e.target.value)} />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Endereço:</Form.Label>
                    <Form.Control type='text' name='address' placeholder={company.address} value={address} onChange={e => setAddress(e.target.value)} />
                </Form.Group>
                <Button onClick={update}>Cadastrar</Button> 
            </Form>
        </Container>
        </>
    )
}